var express = require("express");
var router = express.Router();

var GameController = require("./games/GameController");
var HomeController = require("./home/HomeController");

router.get("/", HomeController.index);
router.get("/games", GameController.game_list);
router.post("games/sync", GameController.game_sync);
router.post("games/find", GameController.game_find);

module.exports = router;
