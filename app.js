require('dotenv').load();

var express = require("express");
var cors  = require('cors');
var app = express();
var bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var db = require("./db");
var router = require("./router");

app.use("/", router);

module.exports = app;
