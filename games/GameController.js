var express = require("express");
var moment = require("moment");

var Game = require("./Game");

exports.game_sync = function(req, res) {
  console.log("sync...");

  var games = [];
  var csv = require("fast-csv");

  Game.deleteMany({}, function(err) {
    console.log(err);
  });

  csv
    .fromPath("base.csv", {
      headers: true,
      delimiter: ";"
    })
    .on("data", function(data) {
      new_game = {
        year: parseInt(data.year),
        host: data.host,
        stage: data.stage,
        date: moment(data.date, "DD-MMM-YYYY").format("YYYYMMDD"),
        player_A: data.player_A,
        player_B: data.player_B,
        score_A: parseInt(data.score_A),
        score_B: parseInt(data.score_B)
      };
      games.push(new_game);
    })
    .on("end", function() {
      Game.collection.insert(games, function(err, game) {
        if (err)
          return res
            .status(500)
            .send(
              "There was a problem adding the information to the database."
            );
        res.status(200).send(games);
      });

      console.log("recovery data done!");
    });
};

exports.game_list = function(req, res) {
  console.log("find all");
  Game.find({}, function(err, games) {
    if (err)
      return res.status(500).send("There was a problem finding the games.");
    res.status(200).send(games);
  });
};

exports.game_find = function(req, res) {
  console.log("find with query");
  console.log(req.body.query);
  Game.find(req.body.query, function(err, games) {
    if (err)
      return res.status(500).send("There was a problem finding the games.");
    res.status(200).send(games);
  });
};
