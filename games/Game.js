var mongoose = require("mongoose");
var GameSchema = new mongoose.Schema({
  year: Number,
  host: String,
  stage: String,
  date: String,
  player_A: String,
  player_B: String,
  score_A: Number,
  score_B: Number
});
mongoose.model("Game", GameSchema);

module.exports = mongoose.model("Game");
