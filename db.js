var mongoose = require("mongoose");
var mongodb_url = "mongodb+srv://" +
    process.env.DB_USER +
    ":" +
    process.env.DB_PASS +
    "@cluster0-ntgbl.mongodb.net/test?retryWrites=false";
console.log('connecting database ' + mongodb_url);
mongoose.connect(mongodb_url);
